<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru !</h1> <br>
    <h2>Sign Up Form</h1><br>
    <form action="/welcome" method="post">
        @csrf
        <label>First Name:</label> <br><br>
        <input type="text" name="fname"> <br><br>
        <label>Last Name:</label> <br><br>
        <input type="text" name="lname"> <br><br>
        <label>Gender</label> <br><br>
        <input type="radio" name="wn"> Male <br>
        <input type="radio" name="wn"> Female <br>
        <input type="radio" name="wn"> Other <br><br>
        <label>Nationality:</label> <br><br>
        <select name="nat">
            <option value="ind">Indonesian</option>
            <option value="oth">Singaporean</option>
            <option value="oth">Malaysian</option>
            <option value="oth">American</option>
        </select> <br><br>
        <label>Language Spoken:</label> <br><br>
        <input type="checkbox"> Bahasa Indonesia <br>
        <input type="checkbox"> English <br>
        <input type="checkbox"> Other <br><br>
        <label>Bio:</label> <br><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br><br>
        <input type="submit">
    </form>
</body>
</html>