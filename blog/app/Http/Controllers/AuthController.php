<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view ('page.register');
    }
    public function send(Request $request){

        $firstName = $request['fname'];
        $lastName = $request['lname'];

        return view ('page.welcome',['firstName' => $firstName,'lastName' => $lastName]);

    }
}
